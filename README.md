# @gvrs/eslint-config

![npm (scoped)](https://img.shields.io/npm/v/@gvrs/eslint-config?style=flat-square)
![NPM](https://img.shields.io/npm/l/@gvrs/eslint-config?style=flat-square)
![PRs Welcome](https://img.shields.io/badge/PRs-Welcome-brightgreenn?style=flat-square)

## Setup

### 1) Install (with peer dependencies)

```bash
npx install-peerdeps --dev @gvrs/eslint-config
```

### 2) Extend config

For React:

```diff
extends: [
+ '@gvrs/eslint-config/react'
]
```

Base only

```diff
extends: [
+ '@gvrs/eslint-config/base'
]
```

### 3) Set [parserOptions.project](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/parser#parseroptionsproject)

```diff
{
+ parserOptions: {
+   project: './tsconfig.json'
+ }
}
```

## Additional Documentation

- [CHANGELOG.md](CHANGELOG.md)
- [eslint-config-airbnb-typescript](https://github.com/iamturns/eslint-config-airbnb-typescript)
- [eslint-config-airbnb](https://github.com/airbnb/javascript)
