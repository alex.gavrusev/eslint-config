## [2.0.1](https://gitlab.com/alex.gavrusev/eslint-config/compare/v2.0.0...v2.0.1) (2022-03-07)

# [2.0.0](https://gitlab.com/alex.gavrusev/eslint-config/compare/v1.0.2...v2.0.0) (2022-03-07)


### Features

* support eslint v8 ([790e4c5](https://gitlab.com/alex.gavrusev/eslint-config/commit/790e4c58a2fb0e93c62b38c2b6c86bf948185fb8))
* tweak function compont definition rule ([d854f1c](https://gitlab.com/alex.gavrusev/eslint-config/commit/d854f1ca784b2d8a150d2fa4a5c9a63960c464ea))


### BREAKING CHANGES

* drop support for eslint v7

## [1.0.2](https://gitlab.com/alex.gavrusev/eslint-config/compare/v1.0.1...v1.0.2) (2022-03-07)


### Bug Fixes

* remove engine requirement ([c7a83dc](https://gitlab.com/alex.gavrusev/eslint-config/commit/c7a83dcc672e283b092c0bcba1718234b28206fe))

## [1.0.1](https://gitlab.com/alex.gavrusev/eslint-config/compare/v1.0.0...v1.0.1) (2022-03-06)


### Bug Fixes

* add license ([2a4d869](https://gitlab.com/alex.gavrusev/eslint-config/commit/2a4d869f0173a1522883455307fcc50b03934dba))

# 1.0.0 (2022-03-06)


### Features

* initial commit ([2637ea1](https://gitlab.com/alex.gavrusev/eslint-config/commit/2637ea1107669f09c13882666dc21286092f6c3e))
