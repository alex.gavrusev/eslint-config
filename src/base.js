module.exports = {
  extends: [
    "eslint-config-airbnb/base",
    "eslint-config-airbnb-typescript/base",
    "./lib/shared",
  ].map(require.resolve),
};
