module.exports = {
  extends: [
    "eslint-config-airbnb",
    "eslint-config-airbnb/hooks",
    "eslint-config-airbnb-typescript",
    "./lib/shared",
  ].map(require.resolve),
  env: {
    browser: true,
    node: true,
  },
  rules: {
    "react/function-component-definition": [
      "error",
      {
        namedComponents: "arrow-function",
        unnamedComponents: "arrow-function",
      },
    ],

    // Makes code too verbose
    "react/jsx-props-no-spreading": "off",

    // We don't use prop-types, try to convert .jsx to .tsx
    "react/prop-types": "off",
  },
  overrides: [
    {
      files: ["**/*.ts", "**/*.tsx"],
      rules: {
        // Mainly encountered in react-hook-form, works fine there
        "@typescript-eslint/unbound-method": "off",
      },
    },
  ],
};
