module.exports = {
  extends: [
    "plugin:@typescript-eslint/recommended",
    "prettier",
    "plugin:import/typescript",
  ],
  settings: {
    "import/resolver": {
      typescript: {
        project: "(t|j)sconfig.json",
      },
    },
  },
  overrides: [
    {
      files: ["**/*.ts", "**/*.tsx"],
      extends: [
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
      ],
      rules: {
        // Shouldn't be just a warning
        "@typescript-eslint/no-explicit-any": "error",

        // Improves code readability
        "@typescript-eslint/prefer-nullish-coalescing": "error",
      },
    },
    {
      files: ["**/*.js", "**/*.jsx"],
      rules: {
        // TS variants require type checking
        "no-implied-eval": "error",
        "no-new-func": "error",
        "@typescript-eslint/no-implied-eval": "off",

        camelcase: [
          "error",
          { properties: "never", ignoreDestructuring: false },
        ],
        "@typescript-eslint/naming-convention": "off",

        "dot-notation": ["error", { allowKeywords: true }],
        "@typescript-eslint/dot-notation": "off",

        "no-throw-literal": "error",
        "@typescript-eslint/no-throw-literal": "off",

        "no-return-await": "error",
        "@typescript-eslint/return-await": "off",
      },
    },
  ],
  env: {
    es2021: true,
  },
  rules: {
    // `void` used for @typescript-eslint/no-floating-promises
    "no-void": ["error", { allowAsStatement: true }],

    // Harms readability
    "no-plusplus": "off",

    // Omit ForOfStatement from
    // https://github.com/airbnb/javascript/blob/63098cbb6c05376dbefc9a91351f5727540c1ce1/packages/eslint-config-airbnb-base/rules/style.js#L339
    "no-restricted-syntax": [
      "error",
      {
        selector: "ForInStatement",
        message:
          "for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.",
      },
      {
        selector: "LabeledStatement",
        message:
          "Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.",
      },
      {
        selector: "WithStatement",
        message:
          "`with` is disallowed in strict mode because it makes code impossible to predict and optimize.",
      },
    ],

    // Shouldn't error out in some cases (pluck out a prop)
    "@typescript-eslint/no-unused-vars": [
      "error",
      { varsIgnorePattern: "^_.+", argsIgnorePattern: "^_.+" },
    ],

    // Improves code readability
    "@typescript-eslint/prefer-optional-chain": "error",

    // Unnecessary, TS should error out on unhandled type changes
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/explicit-function-return-type": "off",

    // Use type for object definitions
    "@typescript-eslint/consistent-type-definitions": ["error", "type"],
  },
};
